import React, { Component } from 'react';

import './App.css';



class App extends Component {



  constructor(props){

    super(props);

    this.state={

      title: 'React Simple CRUD Application',

      act: 0,

      index: '',

      datas: [] 

    }

  } 



  componentDidMount(){

    this.refs.name.focus();

  }



  fSubmit = (e) =>{

    e.preventDefault();

    console.log('try');



    let datas = this.state.datas;

    let name = this.refs.name.value;

      
      if(this.state.act === 0){   

      let data = {

        name

      }

      datas.push(data);

    }else{                      

      let index = this.state.index;

      datas[index].name = name;

    }    



    this.setState({

      datas: datas,

      act: 0

    });



    this.refs.myForm.reset();

    this.refs.name.focus();

  }

  fRemove = (i) => {

    let datas = this.state.datas;

    datas.splice(i,1);

    this.setState({

      datas: datas

    });



    this.refs.myForm.reset();

    this.refs.name.focus();

  }



  fEdit = (i) => {

    let data = this.state.datas[i];

    this.refs.name.value = data.name;

    this.setState({

      act: 1,

      index: i

    });



    this.refs.name.focus();

  }  



  render() {

    let datas = this.state.datas;

    return (

      <div className="App">

        <h2>Приложение to do list!</h2>

          <form ref="myForm" className="myForm">

          <input type="text" ref="name" placeholder="Ваша задача!" className="formField" />
          <button onClick={(e)=>this.fSubmit(e)} className="myButton"> ADD </button>

          </form>

        <pre>
              
          {datas.map((data, i) =>
            

            <li id="list" key={i} className="myList">

              {i+1}. {data.name}
              
              <input id="checkbox" type = "checkbox" className = "myList"></input>
              <label for = "check"></label>
              <button id="button_1" onClick={()=>this.fEdit(i)} className="myList"> Изменить </button>
              <button id="button_2" onClick={()=>this.fRemove(i)} className="myList"> Удалить </button>

              
              
            </li>

          )}

        </pre>

      </div>

    );

  }

}



export default App;